# task-master

Taskmaster helps you outline the tasks you like to complete in a day and provide the functionality to let you check them out as soon as you have completed.